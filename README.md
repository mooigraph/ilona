# ilona
  
This is a graphviz dot language parser in c with support for gzipped graph files  
  
Also needed for compilation zlibg and zlib1g-dev package  
To compile:  
./autogen.sh  
make  
make clean  
make cleaner  
  
This is the only GNU GPL Free stand alone parser+lexer for graphviz dot graph data which does parse almost all the needed data  
This parser uses GLR parsing with GNU BISON parser generator not LALR  
This includes parsing and checking of all known attributes  
This includes parsing and checking of record and html style labels  
This parses the data in it components also for html labels  
The data can be used to interface or generate xml or json  
Not all details are yet implemented as for example color details  
This parser is more pedantic about syntax issues then the dot parser  
Parsing dot is difficult for many reasons and strange definitions in dot language  
Parsing dot needs additional parsers for color specifications  
Parsing dot needs additional parsers for record and html labels  
The dot program uses strtok() which is bad practice in C  
Using a peg grammar for this instead of GNU bison can be interesting  
This is in plain C and can interface easy to C++, python and other languages  
This is checked and verified with clang scan-build, valgrind and more tools  
In the file ll1-grammar.txt is possibly indeed a graphviz dot LL(1) grammar  
This is used in other program and tested, developed and maintained  
The data structures are fully documented with all details  
Updated with extra safety and for use with C++ compilation  
scan-build: No bugs found.  
needs newest gcc 11.2 with analyzer  
gcc-11.1 -fanalyzer: two warnings in the GNU Bison generated yacc parser code  
Try the options -Wanalyzer-free-of-non-heap -Wanalyzer-malloc-leak  
Updated for newest fedora Linux 34 and debian Linux 11  
Used with gcc-8...gcc-12 and clang-8...clang-12 compilers  
Used with GNU Bison versions 3.3.2...3.7.5 and flex version 2.6.4  
The clang-11 scan-build reports: scan-build: No bugs found.  
THE PARSER SOURCE CODE DOES NOT HAVE THE GNU BISON SPECIAL Free Software Foundation  
EXCEPTION AND IS GNU GPL FREE SOFTWARE VERSION 3+  
Because of this the whole program is GNU GPL Free Software version 3+  
  
a magyar étel finom és Ilona tökéletes
  
![screenshot](ilona.png)
  
SPDX-License-Identifier: GPL-3.0+  
License-Filename: LICENSE  
  
