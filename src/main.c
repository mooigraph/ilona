
/*
 *  Copyright 2021
 *
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0+
 * License-Filename: LICENSE
 */

#include "config.h"

#include <stdio.h>
#include <zlib.h>

#include "splay-tree.h"
#include "main.h"
#include "dp.h"
#include "dpmisc.h"
#include "lex.yy.h"

char parsermessage[256];

int main(int argc, char *argv[])
{
	gzFile f;
	int status = 0;
	if (argc) {
	}

	f = gzopen(argv[1], "r");
	if (f == (gzFile) 0) {
		f = gzopen("test.dot", "r");
		/*      return (0); */
	}
	dp_lex_init(f, 1);
	status = yyparse();
	dp_lex_deinit();
	printf("main() %d status `%s'\n", status, dp_errmsg);
	fflush(stdout);
	if (status == 0) {
		status = dp_datachk();
	}
	printf("main() %d status `%s' [status 0 means everything oke]\n", status, dp_errmsg);
	dp_clearall();
	gzclose(f);

	return (0);
}

/* end */
