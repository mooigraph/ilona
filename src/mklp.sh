#!/bin/sh -x
rm dot.tab.c
rm dot.tab.h
rm dot.output
rm dot.xml
rm lex.yy.c
echo "generating dot lexer and parser"

#flex dot.l
flex -Sflex.skl dot.l
bison -d --graph=dot.gv -x dot.y
cat dot.xml |xsltproc xml2dot.xsl - >dot2.gv
cat dot.xml |xsltproc xml2text.xsl - >dot2.txt
cat dot.xml |xsltproc xml2gml.xsl - >dot2.gml
cat dot.xml |xsltproc xml2xhtml.xsl - >dot2.html

echo "generating dot html lexer and parser"
#flex dphl.l
flex -Sflex.skl dphl.l
bison -d --graph=dphl.gv dphl.y
#
echo "flex 2.6.4 lexers generate warnings using scan-build and gcc-11"
flex --version
bison --version
#
diff -p flex.skl flex-2.6.4.skl >flex-skl-diff.txt

#  Using own customized skeleton for the lexer code:
#   flex -S myflex.skl -o mylexer.c mylexer.l
#  Generate the myflex.skl from the development repo with:
#   sed 's/m4_/m4postproc_/g; s/m4preproc_/m4_/g' flex.skl | m4 -P -DFLEX_MAJOR_VERSION=2 -DFLEX_MINOR_VERSION=5 -DFLEX_SUBMINOR_VERSION=33 | sed 's/m4postproc_/m4_/g' > myflex.skl
#  Then edit the myflex.skl and test with needed fixes
# sed 's/m4_/m4postproc_/g; s/m4preproc_/m4_/g' flex-2.4.6.skl | m4 -P -DFLEX_MAJOR_VERSION=2 -DFLEX_MINOR_VERSION=4 -DFLEX_SUBMINOR_VERSION=6 | sed 's/m4postproc_/m4_/g' > myflex.skl

